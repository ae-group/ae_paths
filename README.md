<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project ae.ae V0.3.95 -->
<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project aedev.tpl_namespace_root V0.3.14 -->
# paths 0.3.37

[![GitLab develop](https://img.shields.io/gitlab/pipeline/ae-group/ae_paths/develop?logo=python)](
    https://gitlab.com/ae-group/ae_paths)
[![LatestPyPIrelease](
    https://img.shields.io/gitlab/pipeline/ae-group/ae_paths/release0.3.36?logo=python)](
    https://gitlab.com/ae-group/ae_paths/-/tree/release0.3.36)
[![PyPIVersions](https://img.shields.io/pypi/v/ae_paths)](
    https://pypi.org/project/ae-paths/#history)

>ae_paths module 0.3.37.

[![Coverage](https://ae-group.gitlab.io/ae_paths/coverage.svg)](
    https://ae-group.gitlab.io/ae_paths/coverage/index.html)
[![MyPyPrecision](https://ae-group.gitlab.io/ae_paths/mypy.svg)](
    https://ae-group.gitlab.io/ae_paths/lineprecision.txt)
[![PyLintScore](https://ae-group.gitlab.io/ae_paths/pylint.svg)](
    https://ae-group.gitlab.io/ae_paths/pylint.log)

[![PyPIImplementation](https://img.shields.io/pypi/implementation/ae_paths)](
    https://gitlab.com/ae-group/ae_paths/)
[![PyPIPyVersions](https://img.shields.io/pypi/pyversions/ae_paths)](
    https://gitlab.com/ae-group/ae_paths/)
[![PyPIWheel](https://img.shields.io/pypi/wheel/ae_paths)](
    https://gitlab.com/ae-group/ae_paths/)
[![PyPIFormat](https://img.shields.io/pypi/format/ae_paths)](
    https://pypi.org/project/ae-paths/)
[![PyPILicense](https://img.shields.io/pypi/l/ae_paths)](
    https://gitlab.com/ae-group/ae_paths/-/blob/develop/LICENSE.md)
[![PyPIStatus](https://img.shields.io/pypi/status/ae_paths)](
    https://libraries.io/pypi/ae-paths)
[![PyPIDownloads](https://img.shields.io/pypi/dm/ae_paths)](
    https://pypi.org/project/ae-paths/#files)


## installation


execute the following command to install the
ae.paths module
in the currently active virtual environment:
 
```shell script
pip install ae-paths
```

if you want to contribute to this portion then first fork
[the ae_paths repository at GitLab](
https://gitlab.com/ae-group/ae_paths "ae.paths code repository").
after that pull it to your machine and finally execute the
following command in the root folder of this repository
(ae_paths):

```shell script
pip install -e .[dev]
```

the last command will install this module portion, along with the tools you need
to develop and run tests or to extend the portion documentation. to contribute only to the unit tests or to the
documentation of this portion, replace the setup extras key `dev` in the above command with `tests` or `docs`
respectively.

more detailed explanations on how to contribute to this project
[are available here](
https://gitlab.com/ae-group/ae_paths/-/blob/develop/CONTRIBUTING.rst)


## namespace portion documentation

information on the features and usage of this portion are available at
[ReadTheDocs](
https://ae.readthedocs.io/en/latest/_autosummary/ae.paths.html
"ae_paths documentation").
